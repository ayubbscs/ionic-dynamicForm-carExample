import { FormBuilder, FormGroup } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  carForm: FormGroup;
  make: string;
  model: string;

  constructor(public formBuilder: FormBuilder) {
    this.carForm = formBuilder.group({
      make: [''],
      model: ['']
    });
  }

  save(){
    console.log(this.carForm.value);
  }
}